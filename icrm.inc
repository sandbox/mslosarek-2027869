<?php

/**
 * Returns the config between the Drupal icrm variables and the icrm web services keys 
 * 
 * @return array
 */
function get_icrm_field_mappings() {
	return array(
		'icrm_field_firstname'=>'FirstName',
		'icrm_field_lastname'=>'LastName',
		'icrm_field_streetaddress'=>'StreetAddress',
		'icrm_field_city'=>'City',
		'icrm_field_state'=>'State',
		'icrm_field_zipcode'=>'ZipCode',
		'icrm_field_emailaddress'=>'EmailAddress',
		'icrm_field_birthdate'=>'BirthDate',
		'icrm_field_gender'=>'Gender'
	);
}

/**
 * Checks to see if the path matches a drupal path 
 * 
 * @return string
 */
function icrm_path_check() {
	$valid = TRUE;

	if (!empty($_GET['q'])) {
		if (function_exists('db_query')) {
			$valid = icrm_validate_path_drupal();
		}
	}

	if (!$valid && isset($_GET['q'])) {
		$purl = $_GET['q'];
		$redirectUrl = variable_get('icrm_redirect_url', '/');
		
		$icrm_ticket = icrm_perform_lookup_by_purl($purl);

		if ($icrm_ticket) {
			setCookie('icrmTicket', $icrm_ticket);

			header('location: '.$redirectUrl);
			exit();
		}
	}

	define('ICRM_PATH_CHECKED', TRUE);
}

function icrm_session_check() {
	if (!defined('ICRM_SESSION_CHECKED')) {

		// start the session if it hasen't been already
		drupal_session_start();
		
		if (!isset($_SESSION['icrm'])) {
			$_SESSION['icrm'] = array();
		}

		$icrm_ticket = icrm_get_ticket();
		$icrm_customer = icrm_get_customer();

		define('ICRM_SESSION_CHECKED', TRUE);
	}


	$icrm = $_SESSION['icrm'];

	return $icrm;

}

function icrm_js_check() {
	$install_icrm_js_libs = variable_get('install_icrm_js_libs', 1);
	
	if ($install_icrm_js_libs == 1) {
		if (function_exists('drupal_add_js')) {
			
			$cpmCustomerCode = variable_get('icrm_cpm_customer_code', '');
			$icrmPassword = variable_get('icrm_password', '');
			
			
			if ($cpmCustomerCode && $icrmPassword) {
				$icrmJavascriptUrls = array_filter(explode(',', variable_get('icrm_javascript_url', '')), 'strlen');
			
				if (count($icrmJavascriptUrls) > 1) {
					$weight = 5;
					foreach($icrmJavascriptUrls as $icrmJavascriptUrl) {
						drupal_add_js($icrmJavascriptUrl, array('type' => 'external', 'scope' => 'footer', 'weight' => $weight));
						$weight++;
					}
					drupal_add_js('https://microsite.cpm.com/ICRM/'.$cpmCustomerCode.'/icrm.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 6));		
				} elseif (count($icrmJavascriptUrls) == 1) {
					drupal_add_js('https://webservices.cpm.com/icrm/icrm.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 5));
					drupal_add_js($icrmJavascriptUrls[0], array('type' => 'external', 'scope' => 'footer', 'weight' => 6));
				} else {
					drupal_add_js('https://webservices.cpm.com/icrm/icrm.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 5));
					drupal_add_js('https://microsite.cpm.com/ICRM/'.$cpmCustomerCode.'/icrm.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 6));		
				}
			}
		}	
	}
}

/**
 * Returns the icrm_ticket either from the session or from the cookie, or get a new one
 * 
 * @return string
 */
function icrm_get_ticket() {
	if (!isset($_COOKIE['icrmTicket'])) {
		$_SESSION['icrm']['icrmTicket'] = '';
	} else {
		$_SESSION['icrm']['icrmTicket'] = $_COOKIE['icrmTicket'];
	}

	if (isset($_SESSION['icrm']['icrmTicket'])) {
		$icrm_ticket = $_SESSION['icrm']['icrmTicket'];
	} else {
		$webservices_url = variable_get('webservices_url', 'https://webservices.cpm.com/icrm/');
		$ticket_url = $webservices_url.'ticket/';
		
		$post_variables = array(
			'cpmCustomerCode'  => variable_get('icrm_cpm_customer_code', ''),
			'username'  => variable_get('icrm_username', ''),  
			'password'  => variable_get('icrm_password', ''),
			'neverExpire'  => 1
		);

		$icrm_ticket = icrm_post_response($ticket_url, $post_variables);

		setCookie('icrmTicket', $icrm_ticket);
	}

	$_SESSION['icrm']['icrmTicket'] = $icrm_ticket;
	return $icrm_ticket;
}

/**
 * Get the icrm customer object from the webservices
 * 
 * @return array
 */
function icrm_get_customer() {
	$icrm_ticket = icrm_get_ticket();

	if (!isset($_SESSION['icrm']['customer'])) {
		$_SESSION['icrm']['customer'] = array('Ticket'=>'');
	}
	$icrm_customer = $_SESSION['icrm']['customer'];

	if ($icrm_ticket != $icrm_customer['Ticket']) {
		if ($icrm_ticket) {
			$webservices_url = variable_get('webservices_url', 'https://webservices.cpm.com/icrm/');
			$customer_url = $webservices_url.'customer/';

			$post_variables = array(
				'ticket'  => $icrm_ticket
			);
			$icrm_customer = json_decode(icrm_post_response($customer_url, $post_variables, true), true);

			if (!$icrm_customer) {
				$icrm_customer = array('Ticket'=>'');
			}
		} else {
			$icrm_customer = array('Ticket'=>'');	
		}

		$_SESSION['icrm']['customer'] = $icrm_customer;
		
	}
	return $icrm_customer;
}



/**
 * Perform the icrm lookup on the webservice
 * 
 * @return string
 */
function icrm_perform_lookup_by_purl($purl) {
	$webservices_url = variable_get('webservices_url', 'https://webservices.cpm.com/icrm/', false);
	$ticket_url = $webservices_url.'ticket/';

	$post_variables = array(
		'cpmCustomerCode'  => variable_get('icrm_cpm_customer_code', ''),
		'username'  => variable_get('icrm_username', ''),  
		'password'  => variable_get('icrm_password', ''),
		'neverExpire'  => 1,
		'purl' => $purl
	);

	$icrm_ticket = '';
	$server_response = icrm_post_response($ticket_url, $post_variables);
	$matches = array();
	if (preg_match('/[A-ZA-z0-9]{64}/', $server_response, $matches)) {
		$icrm_ticket = $matches[0];
	}
	
	return $icrm_ticket;
}


/**
 * Check to see if a path works using Drupal MySQL functions (copied from fast_404 module)
 * 
 * @return boolean
 */
function icrm_validate_path_drupal() {

	// Check if path_redirect module is installed.
	if (db_query("SELECT name FROM {system} WHERE type = 'module' AND status = 1 AND name = 'path_redirect'")->fetchField()) {
		// Check if path exits in path_redirect table.
		if (db_query("SELECT rid FROM {path_redirect} WHERE ? LIKE source", array($_GET['q']))->fetchField()) {
			return TRUE;
		}
	}

	$sql = "SELECT path FROM {menu_router} WHERE ? LIKE CONCAT(path, '%')";
	$res = db_query($sql, array($_GET['q']))->fetchField();
	if ($res) {
		return TRUE;
	}
	else {
		$sql = "SELECT pid FROM {url_alias} WHERE ? LIKE CONCAT(alias, '%')";
		$res = db_query($sql, array($_GET['q']))->fetchField();
		return $res == 0 ? FALSE : TRUE;
	}

	return FALSE;
}

/**
 * Fills the form fields with the vales from icrm 
 * 
 * @return null
 */
function populate_inputs(&$root_element, $icrm_values) {
	foreach($root_element as $key=>&$config) {
		if (is_array($config)) {
			if (isset($config['#type']) && $config['#type'] == 'fieldset') {
				populate_inputs($config, $icrm_values);
			} elseif (isset($config['#type']) && in_array($config['#type'], array('textfield', 'radios', 'select', 'textarea'))) {
				if (isset($icrm_values[$key]) && (!isset($config['#default_value']) || !$config['#default_value'])) {
					$config['#default_value'] = $icrm_values[$key];
				}
			} elseif (isset($config['#type'])) {
				pr($key.': '.$config['#type']);
			}
		}
	}
}

/**
 * Perform a post request using either curl or file_get_contents
 * 
 * @return string
 */
function icrm_post_response($url, $post_variables = array(), $requiresPassword=false, $useCurl=true) {
	$server_response = "";
	$icrm_ticket = "";
			
	try {
		if (function_exists('curl_init') && $useCurl) {
			$ch = @curl_init($url);
			
			curl_setopt($ch, CURLOPT_POST,   1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,  $post_variables);

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
			curl_setopt($ch, CURLOPT_HEADER,  0);
			
			if ($requiresPassword) {
				curl_setopt($ch, CURLOPT_USERPWD, variable_get('icrm_username', '').':'.variable_get('icrm_password', ''));
			}

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$server_response = trim(@curl_exec($ch));
			
			$curl_info = curl_getinfo($ch);

			curl_close($ch);

		} elseif (ini_get('allow_url_fopen') == 1) {
			$headers = array(
				"Content-Type: application/x-www-form-urlencoded"
			);

			if ($requiresPassword) {
				$headers[] = "Authorization: Basic ".base64_encode(variable_get('icrm_username', '').':'.variable_get('icrm_password', ''));
			}

			$headers = implode("\r\n", $headers);

			$opts = array(
				'http' => array(
					'method' => 'POST',
					'header' => $headers,
					'content' => http_build_query($post_variables),
					'timeout' => 60
				)
			);

			$server_response = trim(@file_get_contents($url, false, stream_context_create($opts), -1, 40000));
		}
	} catch (Exception $e) {
	}

	return $server_response;
}


function pr() {
	$args = func_get_args();
	echo '<pre>';
	foreach($args as $arg) {
		echo print_r($arg, true)."\n";
	}

	echo '</pre>';
}