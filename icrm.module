<?php
/**
 * @file
 * Add icrm lookup functionality for CPM Healthgrades
 */


/**
* Implements hook_boot().
*/
function icrm_boot() {
	require_once('icrm.inc');

	if (!defined('ICRM_SESSION_CHECKED')) {
		icrm_session_check();
	}
	
	// Don't do a full check if the path was already checked in settings.php.
	if (!defined('ICRM_PATH_CHECKED')) {
		icrm_path_check();
	}
}

/**
* Implements hook_init().
*/
function icrm_init() {
	icrm_js_check();
}

/**
* Implements hook_form_alter().
*/
function icrm_form_alter(&$form, &$form_state, $form_id) {

	$form_mapping_urls = variable_get('icrm_form_mapping', '');

	$isMatched = false;

	if ($form_mapping_urls) {
		$form_mapping_urls = explode(',', $form_mapping_urls);
		foreach($form_mapping_urls as &$form_mapping_url) {
			$form_mapping_url = join('/', array_filter(array_map('trim', explode('/', $form_mapping_url)), 'strlen'));

		}
		$isMatched = in_array(request_path(), $form_mapping_urls);
	}

	if ($isMatched && isset($form['submitted'])) { 
		$icrm_field_mappings = get_icrm_field_mappings();

		$icrm_values = array();
		$icrm_customer = icrm_get_customer();
		
		foreach($icrm_field_mappings as $variable_name=>$icrm_field) {
			$webform_fields = variable_get($variable_name, '');
			$webform_field_array = array_filter(explode(',', $webform_fields), 'strlen');
			foreach($webform_field_array as $webform_field) {
				if (isset($icrm_customer[$icrm_field])) {
					$icrm_values[$webform_field] = $icrm_customer[$icrm_field];
				}
			}
		}

		populate_inputs($form['submitted'], $icrm_values);
	}
}


/**
* Implements hook_admin_settings().
*/
function icrm_admin_settings() {
	return icrm_admin();
}

/**
* Implements hook_admin().
*/
function icrm_admin() {
	$form = array();

	$form['icrm_tabs'] = array(
		'#type' => 'vertical_tabs'	
	);

	$redirect_configuration_tab = array(
		'#type' => 'fieldset',
		'#title' => t('Redirect Configuration'),
	);

	$redirect_configuration_tab['icrm_redirect_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Redirect URL'),
		'#default_value' => variable_get('icrm_redirect_url', '/'),
		'#size' => 30,
		'#maxlength' => 100,
		'#description' => t("The URL to redirect to when ICRM Matches")
	);

	$form['icrm_tabs']['redirect_configuration_tab'] = $redirect_configuration_tab;


	$prepoulation_configuration_tab = array(
		'#type' => 'fieldset',
		'#title' => t('Form Pre-Population Mapping'),
	);

	$prepoulation_configuration_tab['icrm_form_mapping'] = array(
		'#type' => 'textfield',
		'#title' => t('URLs of the forms to maps (separated by commas)'),
		'#default_value' => variable_get('icrm_form_mapping', ''),
		'#size' => 30
	);

	$icrm_field_mappings = get_icrm_field_mappings();
	foreach($icrm_field_mappings as $variable_name=>$icrm_field) {
		$prepoulation_configuration_tab[$variable_name] = array(
			'#type' => 'textfield',
			'#title' => t($icrm_field),
			'#default_value' => variable_get($variable_name, ''),
			'#size' => 30
		);
	}
	$form['icrm_tabs']['prepoulation_configuration_tab'] = $prepoulation_configuration_tab;


	$javascript_configuration_tab = array(
		'#type' => 'fieldset',
		'#title' => t('JavaScript Configuration'),
	);

	$javascript_configuration_tab['install_icrm_js_libs'] = array(
		'#type' => 'checkbox',
		'#title' => t('Install ICRM Javascript Library'),
		'#default_value' => variable_get('install_icrm_js_libs', 1),
		'#description' => t("Automatically install the ICRM Javascript Libraries")
	);

	$form['icrm_tabs']['javascript_configuration_tab'] = $javascript_configuration_tab;


	$login_information_tab = array(
		'#type' => 'fieldset',
		'#title' => t('Login Information'),
	);

	$login_information_tab['icrm_cpm_customer_code'] = array(
		'#type' => 'textfield',
		'#title' => t('CPM Customer Code'),
		'#default_value' => variable_get('icrm_cpm_customer_code', 'ICRM'),
		'#size' => 9,
		'#maxlength' => 6
	);

	$login_information_tab['icrm_username'] = array(
		'#type' => 'textfield',
		'#title' => t('Username'),
		'#default_value' => variable_get('icrm_username', 'icrm'),
		'#size' => 9,
		'#maxlength' => 20
	);

	$login_information_tab['icrm_password'] = array(
		'#type' => 'textfield',
		'#title' => t('Password'),
		'#default_value' => variable_get('icrm_password', ''),
		'#size' => 10,
		'#maxlength' => 30
	);

	$form['icrm_tabs']['login_information_tab'] = $login_information_tab;

	$debug_configuration_tab = array(
		'#type' => 'fieldset',
		'#title' => t('Debug Configuration'),
	);

	$debug_configuration_tab['web_services_url'] = array(
		'#type' => 'textfield',
		'#title' => t('ICRM Webservices URL'),
		'#default_value' => variable_get('web_services_url', 'https://webservices.cpm.com/icrm/'),
		'#size' => 30,
		'#maxlength' => 100,
		'#description' => t("The ICRM Webservices URL, don't change unless instructed.")
	);

	$debug_configuration_tab['icrm_javascript_url'] = array(
		'#type' => 'textfield',
		'#title' => t('ICRM Javascript URL'),
		'#default_value' => variable_get('icrm_javascript_url', ''),
		'#size' => 30,
		'#maxlength' => 200,
		'#description' => t("The ICRM Javascript URL, don't change unless instructed.")
	);

	$form['icrm_tabs']['debug_configuration_tab'] = $debug_configuration_tab;

	return system_settings_form($form);
}

/**
* Implements hook_menu().
*/
function icrm_menu() {

	$items = array();

	$items['admin/config/icrm'] = array(
		'title' => t('ICRM Module Settings'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('icrm_admin_settings'),
		
		'callback' => 'drupal_get_form',
		'callback arguments' => 'icrm_admin',
		
		'access callback' => 'user_access',
		'access arguments' => array('administer site configuration'),
		
		'description' => t('Global configuration of ICRM functionality'),
		'type' => MENU_NORMAL_ITEM
	);

	return $items;
}
